#!/usr/bin/env python
"""RAM rewrite"""
import os
import sys
import time
import subprocess
from argparse import ArgumentParser
from matplotlib import pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.collections import PatchCollection
import numpy as np
import fortranfile
from pyramid import load_namelist
from pyramid import load_units, load_sink, load_map, load_times
from pyramid import C

BIN_FFMPEG = os.environ['HOME']+'/bin/ffmpeg'


class LengthBar(object):
    """Handles length bar"""
    def __init__(self, bar, px_in_cm):
        self.length = int(bar.split(" ")[0])
        self.unit = bar.split(" ")[1]
        self.px_in_cm = float(px_in_cm)

    def in_units(self, unit):
        """Returns barlen in pixels"""
        if unit == 'AU':
            scaling = 1.496e13
        else:
            scaling = C['{}2cm'.format(unit)]
        return self.length/(self.px_in_cm/scaling)


class Camera(object):
    """Handles movie camera angles/distances"""
    def __init__(self, nml, proj, time_now):
        if not nml['run_params']['cosmo']:
            endmov = float(nml['movie_params']['tendmov'])
            startmov = float(nml['movie_params']['tstartmov'])
        else:
            endmov = float(nml['movie_params']['aendmov'])
            startmov = float(nml['movie_params']['astartmov'])

        try:
            self.theta = (nml['movie_params']['theta_camera'][proj]*np.pi/180. +
                          np.min([np.max([time_now-nml['movie_params']['tstart_theta_camera'][proj], 0.]), endmov]) *
                          nml['movie_params']['dtheta_camera'][proj]*np.pi/180. /
                          (endmov-startmov))
        except TypeError:
            self.theta = (nml['movie_params']['theta_camera']*np.pi/180. +
                          np.min([np.max([time_now-nml['movie_params']['tstart_theta_camera'][proj], 0.]), endmov]) *
                          nml['movie_params']['dtheta_camera']*np.pi/180. /
                          (endmov-startmov))

        try:
            self.phi = (nml['movie_params']['phi_camera'][proj]*np.pi/180. +
                        np.min([np.max([time_now-nml['movie_params']['tstart_phi_camera'][proj], 0.]), endmov]) *
                        nml['movie_params']['dphi_camera'][proj]*np.pi/180. /
                        (endmov-startmov))
        except TypeError:
            self.phi = (nml['movie_params']['phi_camera']*np.pi/180. +
                        np.min([np.max([time_now-nml['movie_params']['tstart_phi_camera'][proj], 0.]), endmov]) *
                        nml['movie_params']['dphi_camera']*np.pi/180. /
                        (endmov-startmov))
        try:
            self.dist = (nml['movie_params']['dist_camera'][proj] +
                         np.min([np.max([time_now-nml['movie_params']['tstart_theta_camera'][proj], 0.]),
                                 nml['movie_params']['tend_theta_camera'][proj]]) *
                         nml['movie_params']['ddist_camera'][proj] /
                         (endmov-startmov))
        except TypeError:
            self.dist = (nml['movie_params']['dist_camera'] +
                         np.min([np.max([time_now-nml['movie_params']['tstart_theta_camera'][proj], 0.]),
                                 nml['movie_params']['tend_theta_camera'][proj]]) *
                         nml['movie_params']['ddist_camera'] /
                         (endmov-startmov))

        try:
            self.focal = nml['movie_params']['focal_camera'][proj]
        except TypeError:
            self.focal = nml['movie_params']['focal_camera']

        if self.focal <= 0. or self.focal > self.dist:
            self.focal = self.dist


class MovieFrame(object):
    """Class of RAMSES movie frame"""

    def __init__(self, path):
        ffile = fortranfile.FortranFile(path)
        [self.time, self.fdw, self.fdh, self.fdd] = ffile.readReals('d')
        [self.frame_nx, self.frame_ny] = ffile.readInts()
        self.data = np.array(ffile.readReals(), dtype=np.float64)
        ffile.close()
        self.kind = path.split('/')[-1].split('_')[0]
        if self.kind == 'dens':
            self.unit = 'H/cc'
        elif self.kind == 'temp':
            self.unit = 'K'
        elif self.kind in ['pres', 'pmag']:
            self.unit = 'barye'
        elif self.kind in ['stars', 'dm']:
            self.unit = 'Msun/pc2'
        else:
            self.unit = ''  # default unit
        self.logflag = False

    def setlimits(self, dmin, dmax):
        """Sets limits info of the frame"""
        background = np.where(self.data == 0)

        try:  # setting datamin
            dmin = float(dmin)
            self.data[self.data < dmin] = dmin
            self.plotmin = dmin
        except ValueError:  # argument is invalid
            pass

        try:  # setting datamax
            dmax = float(dmax)
            self.data[self.data > dmax] = dmax
            self.plotmax = dmax
        except ValueError:  # argument is invalid
            pass

        if self.kind in ['vx', 'vy', 'vz']:
            self.plotmax = max(abs(self.plotmin), self.plotmax)
            self.plotmin = -self.plotmax

        self.data[background] = self.plotmin
        self.data = self.data.reshape(self.frame_ny, self.frame_nx)

    def setlogscale(self):
        """Sets logscale and its flag"""
        if self.kind in ['vx', 'vy', 'vz']:
            self.data[np.where(np.isnan(self.data))] = np.nanmin(self.data)
        else:
            try:
                if np.all(self.data <= 0.0):
                    self.data[:, :] = np.log10(np.finfo(np.float32).tiny)
                else:
                    self.data[self.data <= 0.0] = np.nan
                    self.data[np.where(np.isnan(self.data))] = \
                        np.nanmin(self.data)
                    self.data = np.log10(self.data)
            except RuntimeWarning:  # catching and passing
                print('Careful with log! kind={}'.format(self.kind))
        self.logflag = True
        if self.plotmin == 0:
            self.plotmin=np.nanmin(self.data)
        else:
            self.plotmin = np.log10(self.plotmin)
        self.plotmax = np.log10(self.plotmax)

    def scaledata(self, units):
        """Scale data to physical units"""
        px_in_cm = self.fdw/float(self.frame_nx)*units.l
        if self.unit == 'H/cc':
            self.data *= units.d/C['m_H']  # in H/cc
        if self.unit == 'g/cc':
            self.data *= units.d  # in g/cc
        if self.unit == 'Msun/pc3':
            self.data *= units.d/(C['Msun']/C['pc2cm']**3)  # in Msun/pc3
        if self.unit == 'keV':
            self.data *= C['k_B']/C['eV2erg']/1.e3  # in keV
        if self.kind in ["vx", "vy", "vz"]:
            self.data *= (units.l/units.t)/1.e5  # in km/s
        if self.kind == 'var6':  # metals are stored as var6, usually
            self.data /= 0.02  # in Z_solar
        if self.unit == 'barye':
            self.data *= units.d*units.v**2  # in barye
        if self.unit == 'Msun/pc2':
            self.data *= units.inMsun()  # in Solar mass
            self.data /= (px_in_cm/C['pc2cm'])**2
        if self.unit == 'g/cm2':
            self.data *= units.m
            self.data *= px_in_cm**2
       self.plotmin = self.data.min()
       self.plotmax = self.data.max()



def tile_add_colorbar(fig, image, tile_idx, dmin, dmax, geometry,
                      cbar_width, labelcolor, fontsize):
    """Adds colorbar to tile"""
    cbar_format = '%.1f'
    cbaxes = fig.add_axes([1./geometry[1] +
                           tile_idx % geometry[1] *
                           1./geometry[1] -
                           cbar_width / geometry[1],
                           (abs(tile_idx//geometry[1] -
                                geometry[0])-1)/
                           geometry[0],
                           cbar_width / geometry[1],
                           1./geometry[0]])
    cbar = plt.colorbar(image, cax=cbaxes, format=cbar_format,
                        ticks=np.linspace(dmin+0.1*(dmax-dmin),
                                          dmax-0.1*(dmax-dmin),
                                          5))
    cbar.solids.set_rasterized(True)
    cbar.ax.tick_params(width=0, labeltop=True,
                        labelcolor=labelcolor,
                        labelsize=fontsize,
                        pad=-5)
    for tick in cbar.ax.get_yticklabels():
        tick.set_horizontalalignment('right')

    return


def tile_add_lenbar(axis, barry, labelcolor, fontsize, data, units):
    """Adds lenbar to tile"""
    lenbar = LengthBar(barry, data.fdw/float(data.frame_nx)*units.l)
    plt_bar = mpatches.Rectangle((0.025*data.frame_nx, 0.025*data.frame_ny),
                                 lenbar.in_units(lenbar.unit), 10)

    axis.text(0.025+float(lenbar.in_units(lenbar.unit)/data.frame_nx/2.),
              0.025+15./data.frame_ny, "{}".format(barry),
              verticalalignment='bottom',
              horizontalalignment='center',
              transform=axis.transAxes,
              color=labelcolor,
              fontsize=fontsize)

    return plt_bar


def tile_add_label(axis, label, labelcolor, fontsize):
    """Adds label to tile"""
    axis.text(0.85, 0.95, '%s' % label,
              verticalalignment='bottom',
              horizontalalignment='right',
              transform=axis.transAxes,
              color=labelcolor,
              fontsize=fontsize)

    return


def get_frame_centre(nml, proj, time_now):
    """Calculating frame centre"""
    axis = nml['movie_params']['proj_axis'][proj]
    boxlen = nml['amr_params']['boxlen']
    axcycler = {'width': 0, 'height': 1, 'depth': 2}  # z
    if axis == 'x':
        axcycler = {'width': 1, 'height': 2, 'depth': 0}
    elif axis == 'y':
        axcycler = {'width': 0, 'height': 2, 'depth': 1}

    len_proj_axis = len(nml['movie_params']['proj_axis'])
    centre_frame = np.zeros((3, 4*len_proj_axis))

    ax_code = 'xyz'
    for idim in xrange(0, 3):
        for order in xrange(len_proj_axis):  # set default of frame
            centre_frame[idim][4*order] = boxlen/2.
        cf_axis = nml['movie_params'][ax_code[idim]+'centre_frame']
        centre_frame[idim][0:len(cf_axis)] = cf_axis
    del ax_code, cf_axis, len_proj_axis
    frame_centre = np.zeros(3)
    for k in xrange(0, 4):
        frame_centre[0] += (centre_frame[axcycler['width']][4*proj+k]*time_now**k)/boxlen
        frame_centre[1] += (centre_frame[axcycler['height']][4*proj+k]*time_now**k)/boxlen
        frame_centre[2] += (centre_frame[axcycler['depth']][4*proj+k]*time_now**k)/boxlen

    return frame_centre


def get_sink_pos(sink, time_now, cam, nml, proj):
    """Returns sink position on frame"""
    axis = nml['movie_params']['proj_axis'][proj]
    axcycler = {'width': 0, 'height': 1, 'depth': 2}  # z
    if axis == 'x':
        axcycler = {'width': 1, 'height': 2, 'depth': 0}
    elif axis == 'y':
        axcycler = {'width': 0, 'height': 2, 'depth': 1}

    frame_centre = get_frame_centre(nml, proj, time_now)

    xsink = sink.pos[axcycler['width']]-frame_centre[0]
    ysink = sink.pos[axcycler['height']]-frame_centre[1]
    zsink = sink.pos[axcycler['depth']]-frame_centre[2]

    xtmp = np.cos(cam.theta)*xsink+np.sin(cam.theta)*ysink
    ytmp = np.cos(cam.theta)*ysink-np.sin(cam.theta)*xsink
    xsink = xtmp
    ysink = ytmp
    ytmp = np.cos(cam.phi)*ysink+np.sin(cam.phi)*zsink
    ztmp = np.cos(cam.phi)*zsink-np.sin(cam.phi)*ysink
    ysink = ytmp
    zsink = ztmp
    del xtmp, ytmp, ztmp
    try:
        is_perspective = nml['movie_params']['perspective_camera'][proj]
    except TypeError:
        is_perspective = nml['movie_params']['perspective_camera']
    if is_perspective:
        xsink *= cam.focal/(cam.dist-zsink)
        ysink *= cam.focal/(cam.dist-zsink)

    return xsink, ysink, zsink


def assemble_movie(config):
    """Assembles frames into a movie"""
    frame = "{dir}/pngs/{name}_%*.png".format(dir=config.dir,
                                              name=config.config_file.split('.')[0])
    mov = "{dir}/multi.mp4".format(dir=config.dir)

    if config.fname != "":
        mov = "{dir}/{fname}".format(dir=config.dir, fname=config.fname)

    print("Calling ffmpeg! Output: {mov}".format(mov=mov))
    subprocess.call("{binffmpeg} -loglevel quiet -i {input}\
                     -y -vcodec h264 -pix_fmt yuv420p\
                     -r {fps} -filter:v 'setpts={speed}*PTS'\
                     {output}".
                    format(binffmpeg=BIN_FFMPEG, input=frame,
                           fps=config.fps, speed=30./float(config.fps),
                           output=mov), shell=True)
    print("Movie created!")
    subprocess.call("chmod a+r {mov}".format(mov=mov), shell=True)


def make_image(index, config, nml, all_sinks, all_times, units):
    """Making image"""

    if not os.path.exists("{}/movie1/info_{:05d}.txt"
                          .format(config.dir, index)):
        return

    # name the image file
    image_name = ("{}/pngs/{}_{:05d}.png"
                  .format(config.dir, config.config_file.split('.')[0],
                          (index-int(config.fmin))/config.step))

    boxlen = nml['amr_params']['boxlen']
    frame_nx = nml['movie_params']['nw_frame']
    frame_ny = nml['movie_params']['nh_frame']

    if config.keep and os.path.exists(image_name):
        # image is there
        if os.stat(image_name).st_mtime > os.stat(config.config_file).st_mtime:
            # and it's newer than config
            return

    # setting a figure shape
    fig = plt.figure(frameon=False)
    fig.set_size_inches(frame_nx/100.*config.geometry[1],
                        frame_ny/100.*config.geometry[0])

    # need to load units here due to cosmo
    #if nml['run_params']['cosmo']:
    units = load_units(config.dir, index, 1)

    if not nml['run_params']['cosmo']:
        current_aexp = 1.
        current_time = all_times[(index-int(config.fmin))/config.step]
    else:
        current_aexp = all_times[(index-int(config.fmin))/config.step]
        current_time = np.nan

    # looping over tiles in an image
    for tile_idx in xrange(len(config.proj)):

        proj = config.proj[tile_idx]-1
        axis = nml['movie_params']['proj_axis'][proj]

        # load data
        data = MovieFrame("{}/movie{:1d}/{}_{:05d}.map"
                          .format(config.dir,
                                  config.proj[tile_idx],
                                  config.kind[tile_idx],
                                  index))
        rollby = [0, 0]
        if config.roll_axis == 'x':
            rollby[0] = frame_nx/2
            data.data = np.roll(data.data, rollby[0], axis=1)
        if config.roll_axis == 'y':
            rollby[1] = frame_ny/2
            data.data = np.roll(data.data, rollby[1], axis=0)

        data.scaledata(units)
        data.setlimits(config.min[tile_idx],
                       config.max[tile_idx])

        if config.logscale[tile_idx]:
            data.setlogscale()

        ############
        # Plotting #
        ############

        axis = fig.add_subplot(config.geometry[0],
                               config.geometry[1],
                               tile_idx+1)
        axis.axis([0, data.frame_nx, 0, data.frame_ny])
        fig.add_axes(axis)
        # show image
        image = axis.imshow(data.data, interpolation='none',
                            cmap=config.colormap[tile_idx],
                            vmin=data.plotmin, vmax=data.plotmax,
                            aspect='auto')

        # remove ticks
        axis.tick_params(bottom=False, top=False, left=False, right=False)
        axis.tick_params(labelbottom=False, labeltop=False,
                         labelleft=False, labelright=False)

        # add colorbar
        if config.colorbar[tile_idx]:
            tile_add_colorbar(fig, image, tile_idx, data.plotmin, data.plotmax,
                              config.geometry, config.cbar_width,
                              config.labelcolor[tile_idx],
                              config.fontsize[tile_idx])
        # add length bar
        patches = []
        if config.bar[tile_idx]:
            plt_bar = tile_add_lenbar(axis, config.bar[tile_idx],
                                      config.labelcolor[tile_idx],
                                      config.fontsize[tile_idx],
                                      data, units)
            patches.append(plt_bar)
        axis.add_collection(PatchCollection(patches,
                                            facecolor=config.labelcolor[tile_idx]))
        # add label
        if config.label[tile_idx] != "":
            tile_add_label(axis, config.label[tile_idx],
                           config.labelcolor[tile_idx],
                           config.fontsize[tile_idx])

        # add sink
        if nml['run_params']['sink'] and config.sink_flags[tile_idx]:
            sinks_to_plot = []
            sinks = all_sinks[int((index-int(config.fmin))/config.step)]
            if sinks is not None:
                if not nml['run_params']['cosmo']:
                    camera_now = Camera(nml, proj, current_time)
                    time_now = current_time
                else:
                    camera_now = Camera(nml, proj, current_aexp)
                    time_now = current_aexp

                # r_sink = 1/2**(sink level) * ir_cloud(default=4) * boxlen;
                if config.true_sink[tile_idx]:
                    r_sink = (0.5**(sinks[0].level)*4.*boxlen*frame_nx/data.fdw)
                else:
                    r_sink = 10.

                for sink in sinks:
                    xsink, ysink, zsink = get_sink_pos(sink, time_now,
                                                       camera_now, nml, proj)

                    # adding sink to to-plot list if fits in depth
                    if abs(zsink) < data.fdd/boxlen/2.:
                        sinks_to_plot.append(plt.Circle((xsink/(data.fdw/boxlen/2.) *
                                                         data.frame_nx/2 +
                                                         data.frame_nx/2-rollby[0],
                                                         ysink/(data.fdh/boxlen/2.) *
                                                         data.frame_ny/2 +
                                                         data.frame_ny/2-rollby[1]),
                                                        r_sink,
                                                        fill=False, lw=2,
                                                        color=config.labelcolor[tile_idx]))

                for sink_circle in sinks_to_plot:  # putting sinks on the image
                    axis.add_artist(sink_circle)

        # add streamlines from magnetic field lines
        if config.kind[tile_idx] == 'pmag' and config.streamlines[tile_idx] is True:
            dat_u = np.array(load_map(config, tile_idx, index,
                                      'b'+'xyz'.replace(axis, "")[0]+'l'))\
                            .reshape(frame_ny, frame_nx)
            dat_v = np.array(load_map(config, tile_idx, index,
                                      'b'+'xyz'.replace(axis, "")[1]+'l'))\
                            .reshape(frame_ny, frame_nx)
            pmagx = np.linspace(0, frame_nx, num=frame_nx, endpoint=False)
            pmagy = np.linspace(0, frame_ny, num=frame_ny, endpoint=False)
            axis.streamplot(pmagx, pmagy, dat_u, dat_v, density=0.25,
                            color=config.labelcolor[tile_idx], linewidth=1.0)

        # add timer
        if config.timer[tile_idx]:
            if nml['run_params']['cosmo']:
                axis.text(0.05, 0.95, 'z={z:.2f}'.format(z=abs(1./current_aexp-1)),
                          verticalalignment='bottom',
                          horizontalalignment='left',
                          transform=axis.transAxes,
                          color=config.labelcolor[tile_idx],
                          fontsize=config.fontsize[tile_idx])
            else:
                if tile_idx == 0:
                    current_time *= units.t/C['s_in_yr']  # time in years
                time_print_set = '%.{}f %s'.format(1)
                if current_time >= 1e3 and current_time < 1e6:
                    scale_t = 1e3
                    t_unit = 'kyr'
                elif current_time > 1e6 and current_time < 1e9:
                    scale_t = 1e6
                    t_unit = 'Myr'
                elif current_time > 1e9:
                    scale_t = 1e9
                    t_unit = 'Gyr'
                    time_print_set = '%.{}f %s'.format(2)
                else:
                    scale_t = 1.
                    t_unit = 'yr'

                axis.text(0.05, 0.95, time_print_set % (current_time/scale_t, t_unit),
                          verticalalignment='bottom',
                          horizontalalignment='left',
                          transform=axis.transAxes,
                          color=config.labelcolor[tile_idx],
                          fontsize=config.fontsize[tile_idx])

    # corrects window extent
    plt.subplots_adjust(left=0., bottom=0.,
                        right=1.+1.0/data.frame_nx, top=1.+1.0/data.frame_ny,
                        wspace=0., hspace=0.)

    # trying to save an image
    try:
        plt.savefig(image_name, dpi=100)
    except DeprecationWarning:  # just a comparison warning
        pass
    plt.close(fig)

    return


def main():
    """Main body - parses arguments, create images and executes ffmpeg"""
    __version__ = "2.0.8"
    advert = """\
             _____ _____ _____
            | __  |  _  |     |
            |    -|     | | | |
            |__|__|__|__|_|_|_|

          RAMSES Animation Maker
   v {version} - 2019/02/13 - P. Biernacki
    """.format(version=__version__)
    print(advert)

    # Parse command line arguments
    parser = ArgumentParser(description="Script to create RAMSES movies")
    parser.add_argument("-c", "--config", dest="config", metavar="VALUE",
                        type=str, help="config file  [%(default)s]",
                        default="./config.ini")
    parser.add_argument("-d", "--debug", dest="debug", action='store_true',
                        help="debug mode [%(default)r]",
                        default=False)
    parser.add_argument("-n", "--ncpu", dest="ncpu", metavar="VALUE",
                        type=int, help="number of CPUs for multiprocessing [%(default)d]",
                        default=1)
    parser.add_argument("-o", "--output", dest="outfile", metavar="FILE",
                        type=str, help="output image file [<map_file>.png]",
                        default=None)

    for k in xrange(len(sys.argv)):
        if sys.argv[k] in ["-c", "--config"]:
            config_file = sys.argv[k+1]
            break
        else:
            config_file = "./config.ini"

    from load_settings import load_settings
    config = load_settings(parser, config_file)

    # load basic info once, instead of at each loop
    nml = load_namelist(config.namelist, config.dir)

    # load units if not cosmo
    if not nml['run_params']['cosmo']:
        units = load_units(config.dir, config.fmax, 1)
    else:
        units = None

    # load sinks
    all_sinks = []
    all_times = []

    # progressbar imports
    try:
        from widgets import Percentage, Bar, ETA
        from progressbar import ProgressBar
        progressbar_avail = True
    except ImportError:
        progressbar_avail = False

    if config.debug:
        progressbar_avail = False

    if not os.path.exists("%s/pngs/" % (config.dir)):
        os.makedirs("%s/pngs/" % (config.dir))

    # creating images
    if progressbar_avail:
        widgets = ['Working...', Percentage(), Bar(marker='#'), ETA()]
        pbar = ProgressBar(widgets=widgets, maxval=(config.fmax-config.fmin)/config.step+1).start()
    else:
        print('Working!')

    for index in xrange(config.fmin, config.fmax+1, config.step):
        if True in config.sink_flags:
            sinks_i = load_sink(config.dir, index, nml['amr_params']['boxlen'])
            all_sinks.append(sinks_i)
        time_i = load_times(config.dir, index, nml['run_params']['cosmo'])
        all_times.append(time_i)

    if config.ncpu > 1:
        import multiprocessing as mp
        results = []
        pool = mp.Pool(processes=config.ncpu)
        for index in xrange(config.fmin, config.fmax+1, config.step):
            results.append(pool.apply_async(make_image,
                                            args=(index, config, nml,
                                                  all_sinks, all_times, units, )))
        while True:
            inc_count = sum(1 for x in results if not x.ready())
            if inc_count == 0:
                break
            if progressbar_avail:
                pbar.update((config.fmax-config.fmin)/config.step+1-inc_count)
            time.sleep(.1)

        pool.close()
        pool.join()

    elif config.ncpu == 1:
        for index in xrange(config.fmin, config.fmax+1, config.step):
            make_image(index, config, nml,
                       all_sinks, all_times, units)
            if progressbar_avail:
                pbar.update((index+1-config.fmin)/config.step)
    else:
        print('Wrong number of CPUs! Exiting!')
        sys.exit()

    if progressbar_avail:
        pbar.finish()

    assemble_movie(config)
    if not config.keep:
        subprocess.call("rm -r {dir}/pngs".format(dir=config.dir), shell=True)

if __name__ == '__main__':
    main()
